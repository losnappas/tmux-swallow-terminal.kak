# tmux-swallow-terminal.kak

A binding, that you'll need to supplement with your shell. You want to make sense of your Kakoune sessions, because this runs `:quit!`, and if you don't connect to the same session, then your changes will be lost. Kakoune wiki has the page on "single session," use that.

## Commands

Update 9/2020: add `require-module tmux-swallow-terminal` somewhere. connect.kak now uses modules so we gotta use them too to not break.

`hungry-swapping-terminal` you will need https://github.com/alexherbo2/connect.kak for this. It doesn't close the original pane, instead hides it and swaps it back when you close the program that was opened.

`hungry-terminal`

`hungry-ranger`

## Plug.kak

```
plug 'https://gitlab.com/losnappas/tmux-swallow-terminal.kak' defer tmux-swallow-terminal %{
	define-command ranger %{
		hungry-swapping-terminal ranger --choosefile=/tmp/rangr && cat /tmp/rangr | xargs :edit
	}
	define-command lazygit %{
		hungry-swapping-terminal lazygit
		nop %sh{
			{
				sleep 0.1
				tmux send-keys '_'
			} > /dev/null 2>&1 < /dev/null &
		}
	}
}
require-module tmux-swallow-terminal
```
