provide-module tmux-swallow-terminal %{
	require-module connect
	define-command hungry-terminal -params 1.. %{
		nop %sh{
			command tmux split-window -bh "$@" > /dev/null
		}
		execute-keys '<esc>: quit!<ret>'
	}

	define-command hungry-ranger %{
		evaluate-commands %sh{
			if test -z $kak_client_env_is_kak_ranger ; then
				echo 'hungry-terminal "export is_kak_ranger=""1"" VISUAL=""kak -c %val{session}"" EDITOR=""kak -c %val{session}"" && ranger %val{client_env_PWD}"'
			else
				echo 'quit!'
			fi
		}
	}

	define-command hungry-swapping-terminal -params .. %{
		evaluate-commands %sh{
			# Termporarily alias "terminal" so "connect-terminal" creates new window.
			printf "%s\n" "alias window terminal tmux-terminal-window"
			if [ $# -eq 0 ]
			then
				cmd="$SHELL"
			else
				cmd="$@"
			fi
			# Mark current pane
			tmux select-pane -m
			# run `connect $CMD`; and when it finishes, swap the original pane back.
			printf "%s\n" "connect-terminal sh -c '$cmd; tmux join-pane -b'; nop %sh(tmux swap-pane ; tmux select-pane -m ; tmux last-window)"
			printf "%s\n"  "unalias window terminal tmux-terminal-window"
		}

	}
}
